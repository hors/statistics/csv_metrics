# csv_metrics - Compute simple statistical measures for grouped CSV rows
#
# Copyright 2021 Heikki Orsila
#
# SPDX-License-Identifier: BSD-2-Clause

import argparse
from collections import defaultdict
import csv
import re
import statistics


class ArgumentError(Exception):
    pass


def _percentile(values, p):
    values = sorted(values)
    n = len(values) - 1
    if n < 0:
        raise ValueError('Empty values for percentile')
    return values[(n * p) // 100]


VALID_METRICS = set(['max', 'mean', 'median', 'min', 'mode', 'n', 'p10', 'p50',
                     'p90'])


def _statistics(rows, column, converter_map, metrics_set):
    values = [converter_map[column](row[column]) for row in rows]
    d = {
        'max': max(values),
        'mean': statistics.mean(values),
        'median': statistics.median(values),
        'min': min(values),
        'mode': statistics.mode(values),
        'n': len(values),
        'p10': _percentile(values, 10),
        'p50': _percentile(values, 50),
        'p90': _percentile(values, 90),
        }
    # Stupid: we compute all statistics, and then filter what is not needed
    if len(metrics_set) > 0:
        for key in list(d):
            if key not in metrics_set:
                d.pop(key)
    return d


def _split_op(filter_spec: str, op: str):
    fields = filter_spec.split(op)
    assert(len(fields) == 2)
    key = fields[0].strip()
    filter_operand = fields[1].strip()
    return key, filter_operand


def _create_filter_func(filter_spec: str):
    if '!~' in filter_spec:
        key, filter_operand = _split_op(filter_spec, '!~')
        filter_regex = re.compile(filter_operand)

        def filter_func(s: str):
            return filter_regex.match(s) is None

    elif '~' in filter_spec:
        key, filter_operand = _split_op(filter_spec, '~')
        filter_regex = re.compile(filter_operand)

        def filter_func(s: str):
            return filter_regex.match(s) is not None

    elif '=' in filter_spec:
        key, filter_operand = _split_op(filter_spec, '=')

        def filter_func(s: str):
            return s == filter_operand

    else:
        raise ArgumentError('Expected =, ~ or !~ in filter, '
                            'but got: {}'.format(filter_spec))
    return key, filter_func


def _read_text_file(f):
    lines = []
    columns = []
    for line in f.readlines():
        fields = line.split()
        lines.append(fields)
        while len(columns) < len(fields):
            columns.append('column{}'.format(len(columns)))

    rows = []
    for fields in lines:
        row = {column: '' for column in columns}
        for i, field in enumerate(fields):
            column = 'column{}'.format(i)
            row[column] = field
        rows.append(row)

    return rows


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('csv', nargs='*')
    parser.add_argument(
        '--column-mode', action='store_true')
    parser.add_argument(
        '--concatenate', action='store_true',
        help=('Concatenate all CSV files for analysis. '
              'Columns must be compatible.'))
    parser.add_argument(
        '--filter', default=[], action='append',
        help=('Given --filter COLUMN=VALUE, ignore rows where COLUMN is not '
              'VALUE. This argument can be given multiple times.'))
    parser.add_argument(
        '--group-by', default='',
        help='Comma separated columns that group rows into value sets')
    parser.add_argument('--no-print-column', action='store_true')
    parser.add_argument('--no-print-group', action='store_true')
    parser.add_argument('--text-mode', action='store_true')
    parser.add_argument(
        '--value-by', default='',
        help=(
            'Compute statistics for given comma separated columns. '
            'If not given, use all '
            'columns that are not given with --group-by.'))
    parser.add_argument(
        '--metrics', default='',
        help=('Use given comma separated metrics. '
              'If not given, compute all metrics. '
              'Valid values are: {}'.format(sorted(VALID_METRICS))))
    args = parser.parse_args()

    group_columns = []
    for column in args.group_by.split(','):
        column = column.strip()
        if len(column) > 0:
            group_columns.append(column)
    group_columns_set = set(group_columns)

    value_columns = []
    if len(args.value_by) == '':
        value_columns = []
    else:
        for column in args.value_by.split(','):
            column = column.strip()
            if len(column) > 0:
                value_columns.append(column)
    value_columns_set = set(value_columns)

    if len(value_columns_set.intersection(group_columns_set)) > 0:
        raise ArgumentError(
            '--group-by and --value-by columns must be distinct sets')

    metrics = []
    if len(args.metrics) == 0:
        metrics = sorted(VALID_METRICS)
    else:
        for metric in args.metrics.split(','):
            metric = metric.strip()
            if len(metric) == 0:
                continue
            if metric not in VALID_METRICS:
                raise ArgumentError('{} is not a valid metrics. Valid metrics '
                                    'are: {}.'.format(metric, VALID_METRICS))
            metrics.append(metric)

    if len(metrics) == 0:
        raise ArgumentError('No metrics given with --metrics')

    metrics_set = set(metrics)

    filters = []
    for filter_spec in args.filter:
        key, filter_func = _create_filter_func(filter_spec)
        filters.append((key, filter_func))

    BOOL_SET = set(['false', 'true'])

    unfiltered_rows_from_files = []  # Contains unfiltered rows from all files
    columns_set = None
    columns = None

    for fname in args.csv:
        with open(fname, newline='') as csvfile:
            if len(args.csv) > 0:
                print('Reading data from', fname)

            if args.text_mode:
                unfiltered_rows = _read_text_file(csvfile)
            else:
                reader = csv.DictReader(csvfile)
                unfiltered_rows = list(reader)
            if len(unfiltered_rows) == 0:
                print('Nothing to analyze in', fname)
                continue

            if args.concatenate:
                if columns_set is not None:
                    if set(unfiltered_rows[0]) != columns_set:
                        raise ValueError('In --concatenate mode, columns must '
                                         'be identical in all files.')

                if len(unfiltered_rows_from_files) == 0:
                    unfiltered_rows_from_files.append(unfiltered_rows)
                else:
                    unfiltered_rows_from_files[0].extend(unfiltered_rows)
            else:
                unfiltered_rows_from_files.append(unfiltered_rows)

            columns_set = set(unfiltered_rows[0])

    # Do sanity checking for columns
    if columns_set is not None:
        columns = sorted(columns_set)
        for column in group_columns:
            if column not in columns_set:
                raise ArgumentError(
                    'Group by column "{}" is not valid. '
                    'Valid columns are: {}'.format(column, columns))

        for column in value_columns:
            if column not in columns_set:
                raise ArgumentError(
                    'Value by column "{}" is not valid. '
                    'Valid columns are: {}'.format(column, columns))

        for column, unused_filter_func in filters:
            if column not in columns_set:
                raise ArgumentError('Filter column {} is not valid.'.format(
                    column))

    for unfiltered_rows in unfiltered_rows_from_files:
        rows = []
        for row in unfiltered_rows:
            skip = False
            for column, filter_func in filters:
                if not filter_func(row[column]):
                    skip = True
                    break
            if not skip:
                rows.append(row)

        converter_map = {}
        for column in columns:
            is_float = True
            is_bool = True
            for row in rows:
                try:
                    float(row[column])
                except ValueError:
                    is_float = False

                if row[column].lower() not in BOOL_SET:
                    is_bool = False

            if is_float:
                converter_map[column] = float

            if is_bool:
                converter_map[column] = lambda s: float(s.lower() == 'true')

        if len(value_columns) == 0:
            # Note: converter_map is in the same order as columns
            value_columns = list(converter_map)
            value_columns_set = set(value_columns)
        else:
            for column in value_columns:
                if column not in converter_map:
                    raise ArgumentError(
                        'Value by column "{}" does not contain values.'.format(
                            column))

        d = defaultdict(list)
        group_values_to_raw_values = {}
        for row in rows:
            group_values = []
            group_raw_values = []
            for column in group_columns:
                # Convert group value for sorting, if possible
                value = converter_map.get(column, str)(row[column])
                group_values.append(value)
                group_raw_values.append(row[column])
            group_values = tuple(group_values)
            group_raw_values = tuple(group_raw_values)
            group_values_to_raw_values[group_values] = group_raw_values

            d[group_values].append(row)

        group_ids = {}
        group_values_to_group_id = {}
        for group_id, group_values in enumerate(sorted(d)):
            # TODO: Presentation of group values should use
            # non-converted values
            items = []
            group_raw_values = group_values_to_raw_values[group_values]
            for x, y in zip(group_columns, group_raw_values):
                items.append('{}={}'.format(x, y))
            group_values_str = ', '.join(items)
            group_ids[group_id] = group_values_str
            group_values_to_group_id[group_values] = group_id

        # TODO: Column mode instead of group mode
        column_ids = {}
        printed_values = {}
        for i, column in enumerate(columns):
            if column in group_columns:
                continue
            if column not in value_columns_set:
                continue
            column_ids[i] = column

        for group_values, subset_rows in sorted(d.items()):
            group_id = group_values_to_group_id[group_values]
            for column_id, column in enumerate(columns):
                if column in group_columns:
                    continue
                if column not in value_columns_set:
                    continue
                printed_values[(group_id, column_id)] = _statistics(
                    subset_rows, column, converter_map, metrics_set)

        if args.column_mode:
            order = sorted(printed_values, key=lambda p: (p[1], p[0]))
        else:
            order = sorted(printed_values)

        cur_group_id = None
        for group_id, column_id in order:
            value = printed_values[(group_id, column_id)]
            if cur_group_id != group_id:
                if not args.no_print_group:
                    print('Group:', group_ids[group_id])
                cur_group_id = group_id
            if args.no_print_column:
                print(value)
            else:
                print('{}: {}'.format(column_ids[column_id], value))


if __name__ == '__main__':
    main()
